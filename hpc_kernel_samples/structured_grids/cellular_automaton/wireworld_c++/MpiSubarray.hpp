#pragma once
#include <cstddef>
#include <initializer_list>
#include <mpi.h>
#include <vector>

struct SubarrayDimensionDefinition { // helper container
	std::size_t size{};
	std::size_t subSize{};
	std::size_t start{};
};

class SubarrayDefinition { // helper container for MPI Datatype creation
	std::vector<int> sizes_;
	std::vector<int> subSizes_;
	std::vector<int> starts_;

  public:
	auto dims() const { return sizes_.size(); }
	auto sizes() { return sizes_.data(); }
	auto subSizes() { return subSizes_.data(); }
	auto starts() { return starts_.data(); }

	SubarrayDefinition(
	    std::initializer_list<SubarrayDimensionDefinition> saDimDefs);
};

class MpiSubarray { // wrapper for creating and destroying the type
	MPI_Datatype type_{MPI_DATATYPE_NULL};

  public:
	void swap(MpiSubarray& first, MpiSubarray& second) noexcept;
	MPI_Datatype type() const { return type_; }

	MpiSubarray(SubarrayDefinition sd);
	~MpiSubarray();
	MpiSubarray(MpiSubarray&) = delete;
	MpiSubarray& operator=(MpiSubarray&) = delete;
	MpiSubarray(MpiSubarray&& other) noexcept;
	MpiSubarray& operator=(MpiSubarray&& other) noexcept;
};
