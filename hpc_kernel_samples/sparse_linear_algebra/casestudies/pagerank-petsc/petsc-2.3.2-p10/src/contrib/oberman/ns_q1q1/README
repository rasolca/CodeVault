This code uses bilinear-velocity, bilinear-pressure elements.  Unfortunately that choice of element leads to a singular system.  The reason in the failure of the Ladyzhenskaya-Babuska-Brezzi condition, or the inf-sup condition, which measures the compatibility of choices of element spaces for the velocity and pressure vairables.

In the case dealt with here, the divergence stability condition fails.  There are too many degrees of freedom for the pressure, which means that there exist pressures p, so that the weak divergence operator B(u,p) = 0 for all choices of u in the velocity space.
Consequently, the system is singular.

It is easy to see why this happens in out case.  On an NxN grid, we have N^2 nodes.  After applying, for example, Dirichlet boundary conditions for the velocity on 3 walls, and Neumann on one, there are N^2 - 3N +2 free velocity vertices.  After applying Neumann conditions on 3 walls and Dirichlet on one for the pressure, we get N^2 - N free nodes for the pressure. 
Leaving out some details, we have a system with more unknowns that equations for the pressure.

The answer is to go to cell-centered pressure spaces.  For this element-pair, the inf-sup condition continues to fail, but not as badly.  Now there are only a few p's in the space for which B(u,p) = 0 for all u, and these can be filtered out if necessary.  One example is the checkerboard pressure.  In general it is accepted that the method yields good results for the velocity, and the related physical quantities which motivate this application.




