
# ==================================================================================================
# This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
# CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
#
# Author(s):
#   Cedric Nugteren <cedric.nugteren@surfsara.nl>
#
# ==================================================================================================

# FFT with the CUDA cuFFT library
set(NAME ${DWARF_PREFIX}_cufft)
if (CUDA_FOUND)
  cuda_add_executable(${NAME} cufft/cufft.cu)
  target_link_libraries(${NAME} ${CUDA_CUFFT_LIBRARIES})
  install(TARGETS ${NAME} DESTINATION bin)
  message("** Enabling '${NAME}'")
else()
  message("** Skipping '${NAME}': no CUDA")
  install(CODE "MESSAGE(\"${NAME} can only be built with CUDA.\")")                                     
endif()
unset(NAME)

# ==================================================================================================
