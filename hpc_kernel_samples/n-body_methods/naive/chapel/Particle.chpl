use Vec3;

record Particle {
	var mass: real;
	var location: Vec3;
	var velocity: Vec3;
}
