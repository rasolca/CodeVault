#pragma once

#include "Vec3.hpp"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Weverything"
#include <boost/serialization/serialization.hpp>
#pragma clang diagnostic pop

struct Particle {
	double Mass{0};
	Vec3 Location;
	Vec3 Velocity;

	template <class Archive> void serialize(Archive& ar, const unsigned int version) {
		(void)version;
		ar& Mass;
		ar& Location;
		ar& Velocity;
	}

  private:
	friend class boost::serialization::access;
};
BOOST_IS_MPI_DATATYPE(Particle) // performance hint allowed for contiguous data
